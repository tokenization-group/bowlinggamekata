package model;

import infrastructure.ScoreRepository;

public class Game {

    ScoreRepository scoreRepository;

    public Game(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }

    public int score(String playerName) {
        return scoreRepository.getScore(playerName);
    }

    public void roll(String playerName, int knockedPines) {
        int frameScore = this.score(playerName) + knockedPines;
        scoreRepository.register(playerName, frameScore);
    }

}
