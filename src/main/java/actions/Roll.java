package actions;

import model.Game;

public class Roll {

    Game game;

    public Roll(Game game) {
        this.game = game;
    }

    public void execute(String playerName, int knockedPines) {
        game.roll(playerName, knockedPines);
    }
}
