package infrastructure;

import java.util.HashMap;
import java.util.Map;

public class ScoreRepository {
    Map<String, Integer> scores = new HashMap<>();

    public int getScore(String playerName) {
       return scores.getOrDefault(playerName, 0);
    }

    public void register(String playerName, int knockedPines) {
        scores.put(playerName,knockedPines );
    }

}
