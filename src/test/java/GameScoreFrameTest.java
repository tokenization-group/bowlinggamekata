import actions.Roll;
import infrastructure.ScoreRepository;
import model.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameScoreFrameTest {

    private int knockedPinesInOneRoll;
    private int knockedPinesInOtherRoll;

    private int expectedRollScore;
    private int expectedFrameScore;

    private Roll rollAction;
    private ScoreRepository scoreRepository;
    private String playerName;
    private Game game;

    @Before
    public void init() {
        playerName = "Cris";
        scoreRepository = new ScoreRepository();
        game = new Game(scoreRepository);
        rollAction = new Roll(game);
        knockedPinesInOneRoll = 5;
        knockedPinesInOtherRoll = 3;
        expectedRollScore = 5;
        expectedFrameScore = 8;
    }

    @Test
    public void knocked_down_pins_in_one_roll_scored_successfully() {
        when_the_player_knocked_pines(playerName, knockedPinesInOneRoll);
        then_the_player_score_is(expectedRollScore);
    }

    @Test
    public void knocked_down_pines_in_a_frame_scored_successfully() {
        given_a_player_and_a_first_roll();
        when_the_player_knocked_pines_with_his_second_roll(playerName, knockedPinesInOtherRoll);
        then_the_player_score_is(expectedFrameScore);
    }

    private void when_the_player_knocked_pines(String playerName, int knockedPines) {
        rollAction.execute(playerName, knockedPines);
    }

    private void then_the_player_score_is(int expectedScore) {
        assertEquals(expectedScore, scoreRepository.getScore(playerName));
    }

    private void given_a_player_and_a_first_roll() {
        game.roll(playerName, knockedPinesInOneRoll);
    }

    private void when_the_player_knocked_pines_with_his_second_roll(String playerName, int knockedPines) {
        when_the_player_knocked_pines(playerName, knockedPines);
    }

}